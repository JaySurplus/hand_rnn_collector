#ifndef __IMU_DATA_DECODE_H__
#define __IMU_DATA_DECODE_H__

#include <stdint.h>
#include <stdbool.h>

__declspec(dllexport) int imu_data_decode_init(void);
int get_raw_acc(int16_t *a);
int get_raw_gyo(int16_t *g);
int get_raw_mag(int16_t *m);
int get_id(uint8_t *user_id);
int get_eular(float *e);
int get_quat(float *q);

__declspec(dllexport) int export_acc(int16_t *a);
__declspec(dllexport) int export_gyo(int16_t *g);
__declspec(dllexport) int export_mag(int16_t *m);
__declspec(dllexport) int export_id(int8_t *user_id);
__declspec(dllexport) int export_eular(float *e);
__declspec(dllexport) int export_quat(float *q);

#endif
