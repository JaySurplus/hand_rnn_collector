import csv
import time
import os


GESTURE_TYPE = 0
NUMBER_OF_GESTURE = 10
OUTPUT_DIR = "data"


def convertor(root_dir, raw_dir, process_dir, file_name):
    """
    save formatted data to txt.
    """
    # ts = time.gmtime()
    # ts_formatted = time.strftime("%Y-%m-%d-%H_%M_%S", ts)
    # save_path = "./%s/%d_%s.txt" % (OUTPUT_DIR, g_type, ts_formatted)
    p_dir = os.path.join(os.getcwd(), root_dir, process_dir)
    r_dir = os.path.join(os.getcwd(), root_dir, raw_dir)
    if not os.path.isdir(p_dir):
        os.makedirs(p_dir)
    with open(os.path.join(r_dir, file_name + ".csv"), newline='') as f:
        g_type = int(file_name[0])
        f.readline()
        with open(os.path.join(p_dir, file_name + ".txt"), "w+") as fr:
            count = 0
            csv_lines = csv.reader(f, delimiter=",")
            for row in csv_lines:
                line = line_constructor(row, g_type)
                fr.write(line + "\n")
                count += 1
        print("Read %d lines." % count)


def line_constructor(l, g_type):
    """
    input type: line structure is: time stamp + quaternion{w,x,y,z} * 8 + {space} * 1 +
                euler_angle{pitch, yaw, roll} * 8 + {space} * 1 + acc{x, y, z} * 8
                t = l[0].split("-")[-1]
    rtype: time stamp + "|m" + {quaternion, euler, acc} * 3 + "|class" + { number of types }
    """
    t = l[0]
    w0 = l[1:5]
    w1 = l[5:9]
    w2 = l[9:13]

    e0 = l[34:37]
    e1 = l[37:40]
    e2 = l[40:43]

    a0 = l[59:62]
    a1 = l[62:65]
    a2 = l[65:68]

    cla = ['0'] * 10
    cla[g_type] = '1'
    res = [[t], ["|m"], w0, e0, a0, w1, e1, a1, w2, e2, a2, ["|class"], cla]
    flat_res = [i for sublist in res for i in sublist]

    return " ".join(flat_res)


if __name__ == "__main__":
    path = "./data/1.csv"
    try:
        if not os.path.isdir(os.path.join(os.getcwd(), OUTPUT_DIR)):
            os.mkdir(os.path.join(os.getcwd(), OUTPUT_DIR))

    except OSError:
        print("Creation of the directory %s failed" % OUTPUT_DIR)
    else:
        print("Successfully created the directory %s" % OUTPUT_DIR)

    # convertor(path)
