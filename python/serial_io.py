import serial
import time
import logging
import struct
import csv
import convertor
import os
import _thread


QUAT = b'\x71'  # Lenght:128, float(4,4,4,4) * 8
EULAR = b'\x72'  # Lenght:48, int(2,2,2) * 8
ACC = b'\x75'  # Length:48, int(2,2,2) * 8
LEADING_SIZE = 6
DATA_ROOT = 'data'
DATA_RAW = 'raw'
DATA_PROCESSED = 'processed'

data = None
timeStamp = 0
timeStamp_pre = 0
fps = 0
state = "state_Idel"
crc_header = [b'\x5A', b'\xA5', b'\x00', b'\x00']
crc = b'\x00'
crc_calculated = b'\xffff'


data_length = {
    "PRE": 4,
    "CRC": 2,
    "ID": 1,
    "QUAT": 128,
    "EULAR": 48,
    "ACC": 48
}

"""
state machines: name represents current state. With a valid
                input, machine will switch to next state.
"""


def read_decode(input_byte):
    global state

    opt = switcher.get(state, idle_Handler)
    opt(input_byte)


def pre_Handler(input_byte):
    global state
    global timeStamp
    if input_byte == ord(b'\x5A'):
        state = "state_Pre"
        timeStamp = time.time()
    else:
        state = "state_Idle"


def type_Handler(input_byte):
    global state
    if input_byte == ord(b'\xA5'):
        state = "state_Type"
    else:
        state = "state_Idle"


def lenLow_Handler(input_byte):
    global state
    global crc_header
    crc_header[2] = input_byte
    state = "state_LenLow"


def lenHigh_Handler(input_byte):
    global state
    global crc_header
    crc_header[3] = input_byte
    state = "state_LenHigh"


def crcLow_Handler(input_byte):
    global state
    global crc
    crc = input_byte
    state = "state_CRCLow"


def crcHigh_Handler(input_byte):
    global state
    global crc
    # crc_binary = ord(crc)
    # crc_binary |= (ord(input_byte) << 8)
    # crc = hex(crc_binary)
    state = "state_CRCHigh"


def data_Handler(input_byte):
    global state
    global data
    data = input_byte
    state = "state_Data"


def idle_Handler(input_byte):
    global state
    state = "state_Idle"


switcher = {
    "state_Idle": pre_Handler,
    "state_Pre": type_Handler,
    "state_Type": lenLow_Handler,
    "state_LenLow": lenHigh_Handler,
    "state_LenHigh": crcLow_Handler,
    "state_CRCLow": crcHigh_Handler,
    "state_CRCHigh": data_Handler,
    "state_Data": idle_Handler
}


def crc16(crc, raw_data, length):

    for i in range(length):
        byte = raw_data[i]
        crc ^= ord(byte) << 8
        for i in range(8):
            tmp = (crc << 1)
            if crc & 0x8000:
                tmp ^= 0x1021
            crc = tmp
    return crc


def get_fps():
    global timeStamp
    global timeStamp_pre

    res = 1. / (timeStamp - timeStamp_pre)
    timeStamp_pre = timeStamp
    return res


def get_quat(data):
    # 128byte = 8 (receivers) * 4 float/recevier * 4 bytes/float
    tmp = [data[i * 16:i * 16 + 16] for i in range(8)]
    try:
        res = [[round(struct.unpack('<f', tmp[i][j*4:j*4+4])[0], 4)
                for j in range(4)] for i in range(8)]
        return res
    except:
        logging.error("Unpack quat data failed.")
        return [[0. for j in range(4)] for i in range(8)]


def get_eular(data):
    # 48byte = 8 (receivers) * 3 int/recevier * 2 bytes/int
    res = []
    try:
        for i in range(8):
            tmp = [struct.unpack('<h', data[i*6:i*6+6][j*2:j*2+2])[0]
                   for j in range(3)]
            tmp[0] /= 100
            tmp[1] /= 100
            tmp[2] /= 10
            res.append(tmp)
        return res
    except:
        logging.error("Unpack eular data failed.")
        return [[0 for j in range(3)] for j in range(8)]


def get_acc(data):
    # 48byte = 8 (receivers) * 3 int16_t/recevier * 2 bytes/int
    tmp = [data[i * 6:i * 6 + 6] for i in range(8)]
    try:
        res = [[struct.unpack('<h', tmp[i][j*2:j*2+2])[0]
                for j in range(3)] for i in range(8)]
        return res
    except:
        logging.error("Unpack quat data failed.")
        return [[0. for j in range(4)] for i in range(8)]


def read_data(data):
    res = []
    head = 0
    tail = data_length["PRE"]
    prefix = data[head: tail]

    head = tail
    tail += data_length["CRC"]
    crc = data[head: tail]

    head = tail + data_length["ID"]
    tail = head + data_length["QUAT"]
    quat = data[head: tail]

    head = tail + data_length["ID"]
    tail = head + data_length["EULAR"]
    eular = data[head: tail]

    head = tail + data_length["ID"]
    tail = head + data_length["ACC"]
    acc = data[head: tail]

    formatted_quat = get_quat(quat)
    formatted_eul = get_eular(eular)
    formatted_acc = get_acc(acc)
    res = (formatted_quat, formatted_eul, formatted_acc)
    return res


def to_string(data, n=3):
    if n > 8:
        n = 8
    res = []
    for i in range(n):
        index = i + 1
        quat = data[0][i]
        eular = data[1][i]
        acc = data[2][i]
        s = "Receiver %d: \n\
        Quat: %.5f, %.5f, %.5f, %.5f\n\
        Eular: %.3f, %.3f, %.3f\n\
        Acc: %d, %d, %d " % (index,
                             quat[0],
                             quat[1],
                             quat[2],
                             quat[3],
                             eular[0],
                             eular[1],
                             eular[2],
                             acc[0],
                             acc[1],
                             acc[2])
        res.append(s)
    return "\n".join(res)


def flatten(datas):
    res = []
    for data in list(datas):
        res += [item for sublist in data for item in sublist] + [" "]
    return res


def input_thread(res):
    input()
    res.append(True)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s -[%(levelname)s]: %(message)s',
                        level=logging.DEBUG)
    port = 'COM3'

    gesture = input("Enter Gesture number: ")
    ts = time.gmtime()
    ts_formatted = time.strftime("%Y-%m-%d-%H_%M_%S", ts)

    file_name = "%d_%s" % (int(gesture), ts_formatted)
    raw_path = os.path.join(os.getcwd(), DATA_ROOT, DATA_RAW)

    header_str = "Time,W0, X0, Y0, Z0,W1, X1, Y1, Z1,W2, X2, Y2, Z2,W3, X3, Y3, Z3,W4, X4, Y4, Z4,W5, X5, Y5, Z5,W6, X6, Y6, Z6,W7, X7, Y7, Z7,,P0, R0, Y0,P1, R1, Y1,P2, R2, Y2,P3, R3, Y3,P4, R4, Y4,P5, R5, Y5,P6, R6, Y6,P7, R7, Y7,,X0, Y0, Z0,X1, Y1, Z1,X2, Y2, Z2,X3, Y3, Z3,X4, Y4, Z4,X5, Y5, Z5,X6, Y6, Z6,X7, Y7, Z7,"
    header_str = header_str.split(',')

    if not os.path.isdir(raw_path):
        os.makedirs(raw_path)

    try:
        ser = serial.Serial(port, baudrate=115200,
                            bytesize=8)
        csv_writer = csv.writer(
            open(os.path.join(raw_path, file_name + ".csv"), 'w+', newline=''))
        csv_writer.writerow(header_str)

        #a_list = []
        #_thread.start_new_thread(input_thread, (a_list,))

        while True:
            timeStamp = time.time()
            input_bytes = ser.read_all()
            if len(input_bytes) != 233:
                continue
            if input_bytes[0] != ord(b'\x5a'):
                continue
            if input_bytes[1] != ord(b'\xa5'):
                continue
            logging.info("Fucking all right, FPS is %f" %
                         (1. / (timeStamp - timeStamp_pre)))
            timeStamp_pre = timeStamp
            formatted_data = read_data(input_bytes)
            res_str = to_string(formatted_data)
            flatten_str = flatten(formatted_data)

            formatted_time = time.strftime(
                '%Y-%m-%d_%H:%M:%S', time.localtime(int(timeStamp_pre)))
            csv_writer.writerow([formatted_time] + flatten_str)
        #logging.info("Saving data to %s.txt" % (file_name))
        #convertor.convertor(DATA_ROOT, DATA_RAW, DATA_PROCESSED, file_name)
        #logging.info("Close port %s transmission." % (port))
        # ser.close()
    except KeyboardInterrupt:
        logging.info("\nSaving data to %s.txt" % (file_name))
        convertor.convertor(DATA_ROOT, DATA_RAW, DATA_PROCESSED, file_name)
        time.sleep(3)
        logging.info("Close port %s transmission." % (port))
        ser.close()

    except:
        logging.error("Cannot connect to PORT: %s." % (port))
